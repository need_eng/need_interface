/*
 * seriallin.c
 *
 * Created: 12/17/2015 9:29:29 PM
 *  Author: Tulio
 */ 

#include <samd21g17a.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "Board.h"
#include "NEEDOS.h"
#include "needinterface.h"


#define interface_CLK_FREQUENCY	8000000
#define interface_PRESCALER		1
#define interface_FREQUENCY		57600
#define interface_TOP_VAL		(65536*(1-16*((float)interface_FREQUENCY/(float)interface_CLK_FREQUENCY)))

//global variable
interfaceData_t  interfaceData_array[SERIAL_DATA_ARRAY_SIZE];

/**
 * \brief Copy data package to the TX buffer and starts the intarfaceTransmitData sequence,
 * using the interrupt routine. 
 * \param[in] buffer_ptr The memory area to copy from.
 * \param[out] interface The pointer to the used interface to send data from.
 * \return Returns 1 - [Process started]
 *				   0 - [Unable to start process]
 */
int8_t interface_sendData(uint8_t *buffer_ptr,int size, interfaceFm_t *interface)
{
	
	if(interface->txData_ptr.status_flag == SERIAL_S_IDLE)
	{
	//	memcpy(interface->txData_ptr.data,buffer_ptr,size);
		for(int i=0;i<size;i++)
		{
			interface->txData_ptr.data[i] = *buffer_ptr++;
		}
		interface->txData_ptr.lenght = size;
		interface->txData_ptr.actualPosition = 1;
		interface->txData_ptr.status_flag = SERIAL_S_SENDING;
		interface->interfaceTransmitData(interface->txData_ptr.data[0]) ;
		return  1;
	}
	
	return  0;
}
/**
 * \brief Handles the interrupt Tx routine.
 * \param[out] interface The pointer to the used interface to send data from.
 * \return Returns 0 - [Process has finished]
 *				   1 - [Process continues]
 */
int8_t interface_sendHandler(interfaceFm_t *interface)
{
	if(interface->txData_ptr.status_flag ==  SERIAL_S_SENDING)
	{
		if(interface->txData_ptr.actualPosition >= interface->txData_ptr.lenght)
		{
			interface->txData_ptr.status_flag =  SERIAL_S_IDLE;
			return 0;
		}
		else
		{
			interface->interfaceTransmitData( interface->txData_ptr.data[interface->txData_ptr.actualPosition]);
			interface->txData_ptr.actualPosition++;
		}
	}
	return 1;
}
/**
 * \brief Handles the interrupt Rx routine.
 * \param[in] interface The pointer to the used interface to get data to.
 * \return Returns 0 - [Lost flag]
 *				   1 - [Process has started]
 *				   2 - [Process continues]
 *				   3 - [Process overflow]
 */
int8_t interface_receiveHandler(interfaceFm_t *interface)
{
	
	
	uint8_t receivedData;
	if(interface->rxData_ptr.status_flag == SERIAL_S_IDLE)
	{	
		interface->rxData_ptr.actualPosition =0;
		interface->rxData_ptr.lenght =0;
		interface->rxData_ptr.status_flag =  SERIAL_S_RECEIVING;
		//task_wakeup(TASK_interface_OVERFLOW_IDX); //start task overflow
		return 1;
	}
	
	if(interface->rxData_ptr.status_flag ==  SERIAL_S_RECEIVING)
	{
		interface->interfaceReceiveData(&receivedData);
		interface->rxData_ptr.data[interface->rxData_ptr.lenght] = receivedData;
		interface->rxData_ptr.lenght++;
		return 2;
	/////////////////////RETORNE ALGO PARA REINICIAR O TIMMER	task_reset(TASK_interface_OVERFLOW_IDX);
	}
	
	if(interface->rxData_ptr.status_flag == SERIAL_S_READ)
	{
		interface->interfaceReceiveData(&receivedData); //do a dummy read to empty the register
		return 3;
	}
	
	return 0;
}

/**
 * \brief Sends data stored in the interface register without interrupt, using for loop.
 * \param[in\out] interface The pointer to the used interface to send data from.
 * \return Always returns 0.
 */
char interface_sendDataDummy(interfaceFm_t *interface)
{
	for(int i = 0;i<interface->txData_ptr.lenght;i++)
	{
		interface->interfaceTransmitData(interface->txData_ptr.data[i]);
	}
	return 0;
}
/**
 * \brief Get data from the Rx register until a certain timeout occurs.
 * \param[in] interface The pointer to the used interface to get data to.
 * \return Always returns 0.
 */
char interface_receiveDataDummy(interfaceFm_t *interface, uint32_t tries2Timeout)
{
	uint8_t receivedData;
	uint32_t triesCounter=0;
	interface->rxData_ptr.lenght =0;
	while(1)
	{
		if(interface->interfaceReceiveData(&receivedData))
		{
			interface->rxData_ptr.data[interface->rxData_ptr.lenght] = receivedData;
			interface->rxData_ptr.lenght++;
			triesCounter =0;
		}
		else
		{
			if(triesCounter < tries2Timeout)
			{
				triesCounter++;
			}
			else
			{
				break;
			}
		}
		
	}
	return 0;
}
	