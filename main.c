/*
Made by TPO  09/02/2016
	This file is a **GENERIC** way to use the NEED INTERFACE application, for handling
	communications, without the drag of replicating send and receive methods for every 
	communication path.
	
*/

#include "NEEDOS.h"
#include "needinterface.h"


/** N_INTERFACE_CONSTRUCTOR is used to construc a interface,  in the downside example we can see:
*	interfaceModule -> given name to the interface
*	happ_moduleInterfaceTransmitData -> method used by your hardware application to send a byte.
*	happ_moduleInterfaceTransmitData_callback -> method that will be called  in some part of your code after data has benn sent.
*	happ_moduleInterfaceReceiveData -> method used by your hardware application to receive a byte.
*   happ_moduleInterfaceReceiveData_callback -> method called by the interface handler to treat the received data.
*/
N_INTERFACE_CONSTRUCTOR(interfaceModule, happ_moduleInterfaceTransmitData,happ_moduleInterfaceTransmitData_callback, happ_moduleInterfaceReceiveData, happ_moduleInterfaceReceiveData_callback);

int main(void)
{


	system_init();
	
	while (1)
	{
		
	}
}
//your rx interrupt serial handler
void ##SERIAL_RX_INTERRUPT##(void)
{
		interface_receiveHandler(&interfaceModule);
	//do your uC specific functions in here to clean the flags
}

//your tx interrupt serial handler
void ##SERIAL_TX_INTERRUPT##(void)
{
	interface_sendHandler(&interfaceModule);	
	//do your uC specific functions in here to clean the flags
}
