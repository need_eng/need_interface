/*
 * seriallin.h
 *
 * Created: 12/17/2015 9:29:51 PM
 *  Author: Tulio
 */ 


#ifndef NEEDINTERFACE_H_
#define NEEDINTERFACE_H_
#include "Board.h"
#include <samd21g17a.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>


#define SERIAL_MAX_DATA_SIZE	20 /** Define the maximum size of the array that receives and send data. */
#define SERIAL_DATA_ARRAY_SIZE	15 /** Define the size of the rx tx array for serial communication*/
#define SERIAL_INTERFACE_SIZE	2 /** Define the number of  serial communication interfaces*/

/**
 * @brief Use brief, otherwise the index won't have a brief explanation.
 *
 * Detailed explanation.
 */

typedef enum _SERIAL_FLAG_t {SERIAL_S_SENDING=0, SERIAL_S_IDLE=1, SERIAL_S_RECEIVING=2, SERIAL_S_READ=3} SERIAL_FLAG_t;

typedef enum _SERIAL_DATA_TYPE_t {SERIAL_DATA_RX=0, SERIAL_DATA_TX=1} SERIAL_DATA_TYPE_t;
typedef enum _INTERFACE_STATUS_t{INTERFACE_STATUS_NOTIMPLEMENTED} INTERFACE_STATUS_t;


/**
 * @brief Holds simple data register information.
 */
typedef struct _interfaceData_t
{
	int lenght;
	int actualPosition;
	char data[SERIAL_MAX_DATA_SIZE];
	SERIAL_FLAG_t status_flag;
	SERIAL_DATA_TYPE_t data_type;
}interfaceData_t;


typedef struct _interfaceFm_t
{
	int8_t (*interfaceTransmitData)(uint8_t data);
	void (*interfaceTransmitData_callback)(void);
	int8_t (*interfaceReceiveData)(uint8_t *data);
	void (*interfaceReceiveData_callback)(void);
	interfaceData_t  rxData_ptr;
	interfaceData_t  txData_ptr;
	INTERFACE_STATUS_t interface_status;
}interfaceFm_t;



#define N_INTERFACE_CONSTRUCTOR(el,interfaceTransmitData,interfaceTransmitData_callback,interfaceReceiveData,interfaceReceiveData_callback) interfaceFm_t el = {interfaceTransmitData,interfaceTransmitData_callback,interfaceReceiveData,interfaceReceiveData_callback,\
																																								{0,0,{0},	SERIAL_S_IDLE,SERIAL_DATA_RX},{0,0,{0},	SERIAL_S_IDLE,SERIAL_DATA_TX},INTERFACE_STATUS_NOTIMPLEMENTED}
#define N_INTERFACE_LIST(list) serialInterface_t *list[]

int8_t interface_sendData(uint8_t *buffer_ptr,int size, interfaceFm_t *interface);
int8_t interface_sendHandler(interfaceFm_t *interface);

int8_t interface_receiveHandler(interfaceFm_t *interface);
char interface_receiveDataDummy(interfaceFm_t *interface, uint32_t tries2Timeout);
char interface_sendDataDummy(interfaceFm_t *interface);

#endif /* NEEDINTERFACE_H_ */