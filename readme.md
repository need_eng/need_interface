# About this library
 
This file describes how to implement the concept of interface in your application using the need_interface library
If you´re looking for an easy way to receive, send and treat your information and also in a way that is easily scalable, you can just use **need_interface**  lib or concepts.
This file describes how to implement the concept of interface in your application using the **need_interface** library.

##NEED INTERFACE
Add a comment to this line

**NEED INTERFACE** implements a simple way of handling communications in embedded devices, without the  drag of needing to replicate
this methods every time you start a new project or every time you add  a new communication for your project.
Once you implemented the basic methods for initializing, sending a byte, receiving a byte, it just do the data streaming in/out 
and the callback calling for you.
Another huge advantage is that **NEED INTERFACE** makes use of such low level coding that can be implemented in whatever microcontroller
with or without a preexisting RTOS.

So have fun